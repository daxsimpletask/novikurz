import { BrowserModule } from '@angular/platform-browser';

import { NestoService } from './nesto.service';

import { NgModule } from '@angular/core';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule
  ],
  providers: [NestoService],
  bootstrap: []
})
export class  NestoModule { }
