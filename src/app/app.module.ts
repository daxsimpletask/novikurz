import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { NestoModule } from './nesto.module';

import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { TestComponent } from './test.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    NestoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
