import { Component } from '@angular/core';
import { NestoService } from './nesto.service';

@Component({
  selector: 'test',
  templateUrl: './test.component.html'
})
export class TestComponent {

  constructor(public nesto:NestoService){}
  
}
